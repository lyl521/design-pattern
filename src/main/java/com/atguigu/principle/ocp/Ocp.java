package com.atguigu.principle.ocp;

/**
 * 开闭原则。
 * 对扩展开发， 对修改关闭。
 */
public class Ocp {
    public static void main(String[] args) {
        GraphicEditor graphicEditor = new GraphicEditor();
        graphicEditor.draw(new Rectangle());
        graphicEditor.draw(new Circle());
        graphicEditor.draw(new Triangle());
    }
}

/**
 * 绘图类
 */
class GraphicEditor {
    public void draw(Shape shape){
        shape.draw();
    }
}

abstract class Shape {
    int m_type;
    public abstract void draw();
}


class Rectangle extends Shape {
    public Rectangle() {
        m_type = 1;
    }

    @Override
    public void draw() {
        System.out.println(" 矩形 ");
    }
}

class Circle extends Shape {
    Circle() {
        m_type = 2;
    }
    @Override
    public void draw() {
        System.out.println(" 绘制圆形 ");
    }
}


class Triangle extends Shape {
    Triangle() {
        m_type = 3;
    }
    @Override
    public void draw() {
        System.out.println(" 绘制三角形 ");
    }
}