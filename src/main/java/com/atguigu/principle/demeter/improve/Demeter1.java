package com.atguigu.principle.demeter.improve;

import java.util.ArrayList;
import java.util.List;

public class Demeter1 {

    public static void main(String[] args) {
        SchoolManager schoolManager = new SchoolManager();
        schoolManager.printAllEmployee(new CollegeManager());

    }

}


class EmployeeBase {
    private String id;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

class Employee extends EmployeeBase {
    public Employee() {
    }

    public List<Employee> getAllEmployee() {
        List<Employee> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Employee employee = new Employee();
            employee.setId("id= " + i);
            list.add(employee);
        }
        return list;
    }
}


class ManagerBase {
    private Employee employee;

    public ManagerBase() {
        this.employee = new Employee();
    }

    public List<Employee> getAllEmployee() {
        return employee.getAllEmployee();
    }
}

class CollegeManager {

    private Employee employee;

    public CollegeManager() {
        this.employee = new Employee();
    }

    public List<Employee> getAllEmployee() {
        return employee.getAllEmployee();
    }

    public void printEmployee() {
        List<Employee> list1 = getAllEmployee();
        System.out.println("------------------------");
        for (Employee e : list1) {
            System.out.println(e.getId());
        }
    }
}


class SchoolManager {
    public List<Employee> getAllEmployee() {
        List<Employee> list = new ArrayList<Employee>();
        for (int i = 0; i < 5; i++) {
            Employee emp = new Employee();
            emp.setId("id= " + i);
            list.add(emp);
        }
        return list;
    }


    void printAllEmployee(CollegeManager sub) {
        sub.printEmployee();
        List<Employee> list2 = this.getAllEmployee();
        System.out.println("------------------------");
        for (Employee e : list2) {
            System.out.println(e.getId());
        }
    }
}
