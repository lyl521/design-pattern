package com.atguigu.principle.liskov;

/**
 * 里氏替换原则、不重写父类的方法。技巧是，
 * （1）找出重复代码，放在一个base类。
 * （2）把需要依赖的类，以组合（成员变量）的方式使用。
 *  把重复的方法， 代码重构到一个更加基础的 Base 类。 这样就解决了 class A 和 B 的耦合问题。
 */
public class Liskov {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A a = new A();
		System.out.println("11-3=" + a.func1(11, 3));
		System.out.println("1-8=" + a.func1(1, 8));

		System.out.println("-----------------------");
		B b = new B();

		System.out.println("11+3=" + b.func1(11, 3));
		System.out.println("1+8=" + b.func1(1, 8));
		System.out.println("11+3+9=" + b.func2(11, 3));
		
		

		System.out.println("11-3=" + b.func3(11, 3));
		

	}

}

// 这是比 class A 和 B ，都要基础的基类。
class Base {
	// 重复的方法，或者叫做最基础的方法。
}


class A extends Base {

	public int func1(int num1, int num2) {
		return num1 - num2;
	}
}


class B extends Base {
	// 这里class A 作为 class B 的私有属性。 把A组合到B。
	private A aClass = new A();

	public int func1(int a, int b) {
		return a + b;
	}

	public int func2(int a, int b) {
		return func1(a, b) + 9;
	}

	public int func3(int a, int b) {
		return aClass.func1(a, b);
	}
}
