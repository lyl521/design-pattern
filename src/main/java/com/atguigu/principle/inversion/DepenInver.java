package com.atguigu.principle.inversion;

/**
 * 依赖倒转 原则
 */
public class DepenInver {
    public static void main(String[] args) {
        new Person().receive(new Email());
    }


}

class Email {
    public String getInfo() {
        return "电子邮件信息: hello,world";
    }
}

class Person {
    /**
     * 这个方法再使用过程中，很受限制。把接口传入参数，更好。体现多态。
     * @param email
     */
    public void receive(Email email) {
        System.out.println(email.getInfo());
    }
}