package com.atguigu.factory.factorymethod.pizzastore.pizza;

/**
 * 抽象类
 */
public abstract class Pizza {
    protected String name;

    /**
     * 抽象方法
     * 各个子类的不同之处就在这里
     */
    public abstract void prepare();

    public void bake(){}
    public void cut(){}
    public void box(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
