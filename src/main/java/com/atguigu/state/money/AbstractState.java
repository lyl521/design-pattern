package com.atguigu.state.money;

public abstract class AbstractState implements State{

    protected static final RuntimeException EXCEPTION = new RuntimeException("操作流程不允许");

    @Override
    public void checkEvent(Context context) {
        throw EXCEPTION;
    }

    @Override
    public void checkFailEvent(Context context) {

    }

    @Override
    public void makePriceEvent(Context context) {

    }

    @Override
    public void acceptOrderEvent(Context context) {

    }

    @Override
    public void notPeopleAcceptEvent(Context context) {

    }

    @Override
    public void payOrderEvent(Context context) {

    }

    @Override
    public void orderFailureEvent(Context context) {

    }

    @Override
    public void feedBackEvent(Context context) {

    }


}
