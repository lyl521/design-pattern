package com.atguigu.strategy;

public interface FlyBehavior {
    void fly();
}
