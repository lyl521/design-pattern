package com.atguigu.singleton;

/**
 * 枚举
 */
public class SingletonTest08 {
    public static void main(String[] args) {
        Singleton08 instance1 = Singleton08.INSTANCE;
        Singleton08 instance2 = Singleton08.INSTANCE;

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() + "\t" + instance2.hashCode());
    }

}


enum Singleton08{
    INSTANCE,
    ;
    public void sayOK() {
        System.out.println("ok~");
    }
}