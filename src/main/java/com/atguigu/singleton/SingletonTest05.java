package com.atguigu.singleton;

public class SingletonTest05 {
    public static void main(String[] args) {
        Singleton05 instance1 = Singleton05.getInstance();
        Singleton05 instance2 = Singleton05.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() + "\t" + instance2.hashCode());
    }

}


class Singleton05 {
    // 构造函数私有化
    private Singleton05() {
    }

    private static Singleton05 instance;

    // 使用同步代码块，但是仍旧不能保证线程安全。不适用。
    public static Singleton05 getInstance() {
        if (instance == null) {
            synchronized (Singleton05.class){
                instance = new Singleton05();
            }
        }
        return instance;
    }

}