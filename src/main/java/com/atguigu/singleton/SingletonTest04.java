package com.atguigu.singleton;

public class SingletonTest04 {
    public static void main(String[] args) {
        Singleton04 instance1 = Singleton04.getInstance();
        Singleton04 instance2 = Singleton04.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() +"\t"+ instance2.hashCode());
    }

}


class Singleton04 {
    // 构造函数私有化
    private Singleton04(){}
    private static Singleton04 instance;

    // 加入同步处理的代码，解决线程安全问题.
    public static synchronized Singleton04 getInstance(){
        if(instance==null)
            instance = new Singleton04();
        return instance;
    }

}