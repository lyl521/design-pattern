package com.atguigu.singleton;

public class SingletonTest02 {
    public static void main(String[] args) {
        Singleton02 instance1 = Singleton02.getInstance();
        Singleton02 instance2 = Singleton02.getInstance();

        System.out.println(instance1 == instance2);
        System.out.println(instance1.hashCode() +"\t"+ instance2.hashCode());
    }

}

/**
 * 在静态代码块中，创建单例对象.
 */
class Singleton02 {
    // 构造函数私有化
    private Singleton02(){}
    private static Singleton02 instance;
    static {
        instance = new Singleton02();
    }
    public static Singleton02 getInstance(){
        return instance;
    }

}