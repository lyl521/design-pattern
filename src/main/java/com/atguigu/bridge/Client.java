package com.atguigu.bridge;

public class Client {
    public static void main(String[] args) {
        FoldedPhone phone1 = new FoldedPhone(new Xiaomi());
        phone1.open();
        phone1.call();
        phone1.close();
        System.out.println();
    }
}
