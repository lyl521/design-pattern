package com.atguigu.iterator;

import java.util.Iterator;

public class ComputerCollegeIterator implements Iterator {
    Department[] departments;
    int position = 0; //遍历的位置

    public ComputerCollegeIterator(Department[] departments) {
        this.departments = departments;
    }

    @Override
    public boolean hasNext() {
        return !(position>=departments.length || departments[position]==null);
    }

    @Override
    public Object next() {
        Department department = null;
        if(hasNext()){
            department = departments[position];
            position++;
        }
        return department;
    }

    //删除的方法，默认空实现
    public void remove() {

    }

}
