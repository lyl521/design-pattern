package com.atguigu.decorator;

public class Decorator extends Drink {
    Drink obj;

    public Decorator(Drink drink) {
        obj = drink;
    }

    @Override
    public float cost() {
        float v = super.getPrice() + obj.getPrice();
        return v;
    }

    @Override
    public String getDes() {
        return des + " " + getPrice() + " && " + obj.getDes();
    }
}
