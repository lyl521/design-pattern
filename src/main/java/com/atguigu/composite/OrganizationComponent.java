package com.atguigu.composite;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 组合设计模式
 * 使用场景：树状的结构，组织系统。
 */
@Data
@AllArgsConstructor
public abstract class OrganizationComponent {
    private String name; // 名字
    private String des; // 说明

    protected void add(OrganizationComponent organizationComponent){
        // 默认实现
        throw new UnsupportedOperationException();
    }

    protected void remove(OrganizationComponent organizationComponent){
        // 默认实现
        throw new UnsupportedOperationException();
    }

    //方法print, 做成抽象的, 子类都需要实现
    protected abstract void print();

}
