package com.atguigu.interpreter;

import java.util.HashMap;

public class VarExpression extends Expression {

    private String key;

    public VarExpression(String key) {
        this.key = key;
    }

    @Override
    public int interpreter(HashMap<String, Integer> var) {
        // interpreter 根据 变量名称，返回对应值
        return var.get(key);
    }
}
